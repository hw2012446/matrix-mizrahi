const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  const header = req.headers['authorization'];
  if (!header) {
    return res.status(403).send({ message: 'No token provided!' });
  }

  const token = header.trim().split(/\s+/)[1];
  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }

    const currentUnixTime = Math.floor(Date.now() / 1000);
    if (decoded.exp && decoded.exp < currentUnixTime) {
      return res.status(401).send({ message: 'Token expired!' });
    }
    next();
  });
}
module.exports = verifyToken;

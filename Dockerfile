FROM node:16-alpine3.11
WORKDIR /usr/src/app
COPY package*.json ./
ARG NODE_ENV
RUN if [ "$NODE_ENV" = "production" ]; \
    then npm install --only=production; \
    else npm install; \
    fi

COPY . .

RUN if [ "$NODE_ENV" = "production" ]; then rm -rf ./__tests__; fi

CMD if [ "$NODE_ENV" = "production" ]; \
    then node index.js; \
    else npm test; \
    fi

EXPOSE 8080

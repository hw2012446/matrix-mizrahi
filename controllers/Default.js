'use strict';

const utils = require('../utils/writer.js');
const Default = require('../service/DefaultService');
const verifyToken = require('../middleware/jwtMiddleware');

module.exports.calculatePOST = function calculatePOST (req, res, next, body, operation) {
  verifyToken(req, res, function() {
    Default.calculatePOST(body, operation)
      .then(function (response) {
        utils.writeJson(res, response, 200);
      })
      .catch(function (response) {
        utils.writeJson(res, response.message, 400);
      });
  });
};

module.exports.healthcheckGET = function healthcheckGET (req, res, next) {
  Default.healthcheckGET()
    .then(function (response) {
      utils.writeJson(res, response, 200);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

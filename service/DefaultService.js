'use strict';


/**
 * Performs a calculation operation
 * Receives two numeric values and an operation from the header and returns the result
 *
 * body Calculate_body
 * operation String The calculation operation (subtraction, division, multiplication, addition)
 * returns BigDecimal
 **/
exports.calculatePOST = function(body, operation) {
  return new Promise(function(resolve, reject) {
    if (typeof body.value1 !== 'number' || typeof body.value2 !== 'number') {
      return reject(new Error("Invalid input values"));
    }

    let result;
    switch (operation) {
      case 'addition':
        result = body.value1 + body.value2;
        break;
      case 'subtraction':
        result = body.value1 - body.value2;
        break;
      case 'multiplication':
        result = body.value1 * body.value2;
        break;
      case 'division':
        if (body.value2 === 0) {
          return reject(new Error("Division by zero"));
        }
        result = body.value1 / body.value2;
        break;
      default:
        return reject(new Error("Invalid operation"));
    }

    resolve(result);
  });
};



/**
 * Health Check
 * Checks if the server is up and running
 *
 * no response value expected for this operation
 **/
exports.healthcheckGET = function() {
  return new Promise(function(resolve, reject) {
    resolve("OK");
  });
}


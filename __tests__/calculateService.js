const { calculatePOST } = require('../service/DefaultService');

describe('calculatePOST', () => {
    it('correctly adds two numbers', () => {
        return expect(calculatePOST({ value1: 5, value2: 3 }, 'addition')).resolves.toBe(8);
    });

    it('correctly subtracts two numbers', () => {
        return expect(calculatePOST({ value1: 5, value2: 3 }, 'subtraction')).resolves.toBe(2);
    });

    it('correctly multiplies two numbers', () => {
        return expect(calculatePOST({ value1: 5, value2: 3 }, 'multiplication')).resolves.toBe(15);
    });

    it('correctly divides two numbers', () => {
        return expect(calculatePOST({ value1: 6, value2: 3 }, 'division')).resolves.toBe(2);
    });

    it('rejects with invalid input values', () => {
        return expect(calculatePOST({ value1: 'not-a-number', value2: 3 }, 'addition')).rejects.toThrow('Invalid input values');
    });

    it('rejects division by zero', () => {
        return expect(calculatePOST({ value1: 5, value2: 0 }, 'division')).rejects.toThrow('Division by zero');
    });

    it('rejects invalid operations', () => {
        return expect(calculatePOST({ value1: 5, value2: 3 }, 'not-an-operation')).rejects.toThrow('Invalid operation');
    });
});

const request = require('supertest');
const app = require('../app');

describe('POST /calculate', () => {
  it('correctly adds two numbers', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'addition')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 5, value2: 3 });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ code: 200, payload: 8 });
  });

  it('correctly subtracts two numbers', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'subtraction')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 10, value2: 4 });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ code: 200, payload: 6 });
  });

  it('correctly multiplies two numbers', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'multiplication')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 7, value2: 6 });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ code: 200, payload: 42 });
  });

  it('correctly divides two numbers', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'division')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 20, value2: 4 });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ code: 200, payload: 5 });
  });

  it('handles division by zero', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'division')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 15, value2: 0 });

    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({ code: 400, payload: "Division by zero" });
  });

  it('validation value2 param', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'division')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value1: 15 });

    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({"message":"request.body should have required property 'value2'","errors":[{"path":".body.value2","message":"should have required property 'value2'","errorCode":"required.openapi.validation"}]});
  });

  it('validation value1 param', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'division')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value2: 15 });

    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({"message":"request.body should have required property 'value1'","errors":[{"path":".body.value1","message":"should have required property 'value1'","errorCode":"required.openapi.validation"}]});
  });

  it('validation operation header', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('Authorization', `Bearer ${process.env.JWT_TOKEN_TEST}`)
      .send({ value2: 15, value1: 1});

    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({"message":"request.headers should have required property 'operation'","errors":[{"path":".headers.operation","message":"should have required property 'operation'","errorCode":"required.openapi.validation"}]});
  });

  it('validation Authorization header', async () => {
    const res = await request(app)
      .post('/calculate')
      .set('operation', 'division')
      .send({ value2: 15, value1: 1});

    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({"message":"Authorization header required","errors":[{"path":"/calculate","message":"Authorization header required"}]});
  });
});
